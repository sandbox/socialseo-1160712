<?php
/**
 * @file
 * Seomoz api
 *
 */

  define('ANCHOR_SCOPE_PHRASE_TO_PAGE', 'phrase_to_page');
  define('ANCHOR_SCOPE_PHRASE_TO_SUBDOMAIN', 'phrase_to_subdomain');
  define('ANCHOR_SCOPE_PHRASE_TO_DOMAIN', 'phrase_to_domain');
  define('ANCHOR_SCOPE_TERM_TO_PAGE', 'term_to_page');
  define('ANCHOR_SCOPE_TERM_TO_SUBDOMAIN', 'term_to_subdomain');
  define('ANCHOR_SCOPE_TERM_TO_DOMAIN', 'term_to_domain');
  
  define('ANCHOR_SORT_DOMAINS_LINKING_PAGE', 'domains_linking_page');
  
  define('ANCHOR_COL_ALL', '0');
  define('ANCHOR_COL_TERM_OR_PHRASE', '2');
  define('ANCHOR_COL_INTERNAL_PAGES_LINK', '8');
  define('ANCHOR_COL_INTERNAL_SUBDMNS_LINK', '16');
  define('ANCHOR_COL_EXTERNAL_PAGES_LINK', '32');
  define('ANCHOR_COL_EXTERNAL_SUBDMNS_LINK', '64');
  define('ANCHOR_COL_EXTERNAL_ROOTDMNS_LINK', '128');
  define('ANCHOR_COL_INTERNAL_MOZRANK', '256');
  define('ANCHOR_COL_EXTERNAL_MOZRANK', '512');
  define('ANCHOR_COL_FLAGS', '1024');



  define('LINKS_SCOPE_PAGE_TO_PAGE', 'page_to_page');
  define('LINKS_SCOPE_PAGE_TO_SUBDOMAIN', 'page_to_subdomain');
  define('LINKS_SCOPE_PAGE_TO_DOMAIN', 'page_to_domain');
  define('LINKS_SCOPE_DOMAIN_TO_PAGE', 'domain_to_page');
  define('LINKS_SCOPE_DOMAIN_TO_SUBDOMAIN', 'domain_to_subdomain');
  define('LINKS_SCOPE_DOMAIN_TO_DOMAIN', 'domain_to_domain');
  
  define('LINKS_SORT_PAGE_AUTHORITY', 'page_authority');
  define('LINKS_SORT_DOMAIN_AUTHORITY', 'domain_authority');
  define('LINKS_SORT_DOMAINS_LINKING_DOMAIN', 'domains_linking_domain');
  define('LINKS_SORT_DOMAINS_LINKING_PAGE', 'domains_linking_page');
  
  define('LINKS_FILTER_INTERNAL', 'internal');
  define('LINKS_FILTER_EXTERNAL', 'external');
  define('LINKS_FILTER_NOFOLLOW', 'nofollow');
  define('LINKS_FILTER_FOLLOW', 'follow');
  define('LINKS_FILTER_301', '301');
  
  define('LINKS_COL_ALL', '0');
  define('LINKS_COL_TITLE', '1');
  define('LINKS_COL_URL', '4');
  define('LINKS_COL_SUBDOMAIN', '8');
  define('LINKS_COL_ROOT_DOMAIN', '16');
  define('LINKS_COL_EXTERNAL_LINKS', '32');
  define('LINKS_COL_SUBDMN_EXTERNAL_LINKS', '64');
  define('LINKS_COL_ROOTDMN_EXTERNAL_LINKS', '128');
  define('LINKS_COL_JUICE_PASSING_LINKS', '256');
  define('LINKS_COL_SUBDMN_LINKS', '512');
  define('LINKS_COL_ROOTDMN_LINKS', '1024');
  define('LINKS_COL_LINKS', '2048');
  define('LINKS_COL_SUBDMN_SUBDMN_LINKS', '4096');
  define('LINKS_COL_ROOTDMN_ROOTDMN_LINKS', '8192');
  define('LINKS_COL_MOZRANK', '16384');
  define('LINKS_COL_SUBDMN_MOZRANK', '32768');
  define('LINKS_COL_ROOTDMN_MOZRANK', '65536');
  define('LINKS_COL_MOZTRUST', '131072');
  define('LINKS_COL_SUBDMN_MOZTRUST', '262144');
  define('LINKS_COL_ROOTDMN_MOZTRUST', '524288');
  define('LINKS_COL_EXTERNAL_MOZRANK', '1048576');
  define('LINKS_COL_SUBDMN_EXTERNALDMN_JUICE', '2097152');
  define('LINKS_COL_ROOTDMN_EXTERNALDMN_JUICE', '4194304');
  define('LINKS_COL_SUBDMN_DOMAIN_JUICE', '8388608');
  define('LINKS_COL_ROOTDMN_DOMAIN_JUICE', '16777216');
  define('LINKS_COL_CANONICAL_URL', '268435456');
  define('LINKS_COL_HTTP_STATUS_CODE', '536870912');
  define('LINKS_COL_LINKS_TO_SUBDMN', '4294967296');
  define('LINKS_COL_LINKS_TO_ROOTDMN', '8589934592');
  define('LINKS_COL_ROOTDMN_LINKS_SUBDMN', '17179869184');
  define('LINKS_COL_PAGE_AUTHORITY', '34359738368');
  define('LINKS_COL_DOMAIN_AUTHORITY', '68719476736');


  define('TOPPAGES_COL_ALL', '0');
  define('TOPPAGES_COL_TITLE', '1');
  define('TOPPAGES_COL_URL', '4');
  define('TOPPAGES_COL_SUBDOMAIN', '8');
  define('TOPPAGES_COL_ROOT_DOMAIN', '16');
  define('TOPPAGES_COL_EXTERNAL_LINKS', '32');
  define('TOPPAGES_COL_SUBDMN_EXTERNAL_LINKS', '64');
  define('TOPPAGES_COL_ROOTDMN_EXTERNAL_LINKS', '128');
  define('TOPPAGES_COL_JUICE_PASSING_LINKS', '256');
  define('TOPPAGES_COL_SUBDMN_LINKS', '512');
  define('TOPPAGES_COL_ROOTDMN_LINKS', '1024');
  define('TOPPAGES_COL_LINKS', '2048');
  define('TOPPAGES_COL_SUBDMN_SUBDMN_LINKS', '4096');
  define('TOPPAGES_COL_ROOTDMN_ROOTDMN_LINKS', '8192');
  define('TOPPAGES_COL_MOZRANK', '16384');
  define('TOPPAGES_COL_SUBDMN_MOZRANK', '32768');
  define('TOPPAGES_COL_ROOTDMN_MOZRANK', '65536');
  define('TOPPAGES_COL_MOZTRUST', '131072');
  define('TOPPAGES_COL_SUBDMN_MOZTRUST', '262144');
  define('TOPPAGES_COL_ROOTDMN_MOZTRUST', '524288');
  define('TOPPAGES_COL_EXTERNAL_MOZRANK', '1048576');
  define('TOPPAGES_COL_SUBDMN_EXTERNALDMN_JUICE', '2097152');
  define('TOPPAGES_COL_ROOTDMN_EXTERNALDMN_JUICE', '4194304');
  define('TOPPAGES_COL_SUBDMN_DOMAIN_JUICE', '8388608');
  define('TOPPAGES_COL_ROOTDMN_DOMAIN_JUICE', '16777216');
  define('TOPPAGES_COL_CANONICAL_URL', '268435456');
  define('TOPPAGES_COL_HTTP_STATUS_CODE', '536870912');
  define('TOPPAGES_COL_LINKS_TO_SUBDMN', '4294967296');
  define('TOPPAGES_COL_LINKS_TO_ROOTDMN', '8589934592');
  define('TOPPAGES_COL_ROOTDMN_LINKS_SUBDMN', '17179869184');
  define('TOPPAGES_COL_PAGE_AUTHORITY', '34359738368');
  define('TOPPAGES_COL_DOMAIN_AUTHORITY', '68719476736');


  define('URLMETRICS_COL_ALL', '0');
  define('URLMETRICS_COL_TITLE', '1');
  define('URLMETRICS_COL_URL', '4');
  define('URLMETRICS_COL_SUBDOMAIN', '8');
  define('URLMETRICS_COL_ROOT_DOMAIN', '16');
  define('URLMETRICS_COL_EXTERNAL_LINKS', '32');
  define('URLMETRICS_COL_SUBDMN_EXTERNAL_LINKS', '64');
  define('URLMETRICS_COL_ROOTDMN_EXTERNAL_LINKS', '128');
  define('URLMETRICS_COL_JUICE_PASSING_LINKS', '256');
  define('URLMETRICS_COL_SUBDMN_LINKS', '512');
  define('URLMETRICS_COL_ROOTDMN_LINKS', '1024');
  define('URLMETRICS_COL_LINKS', '2048');
  define('URLMETRICS_COL_SUBDMN_SUBDMN_LINKS', '4096');
  define('URLMETRICS_COL_ROOTDMN_ROOTDMN_LINKS', '8192');
  define('URLMETRICS_COL_MOZRANK', '16384');
  define('URLMETRICS_COL_SUBDMN_MOZRANK', '32768');
  define('URLMETRICS_COL_ROOTDMN_MOZRANK', '65536');
  define('URLMETRICS_COL_MOZTRUST', '131072');
  define('URLMETRICS_COL_SUBDMN_MOZTRUST', '262144');
  define('URLMETRICS_COL_ROOTDMN_MOZTRUST', '524288');
  define('URLMETRICS_COL_EXTERNAL_MOZRANK', '1048576');
  define('URLMETRICS_COL_SUBDMN_EXTERNALDMN_JUICE', '2097152');
  define('URLMETRICS_COL_ROOTDMN_EXTERNALDMN_JUICE', '4194304');
  define('URLMETRICS_COL_SUBDMN_DOMAIN_JUICE', '8388608');
  define('URLMETRICS_COL_ROOTDMN_DOMAIN_JUICE', '16777216');
  define('URLMETRICS_COL_CANONICAL_URL', '268435456');
  define('URLMETRICS_COL_HTTP_STATUS_CODE', '536870912');
  define('URLMETRICS_COL_LINKS_TO_SUBDMN', '4294967296');
  define('URLMETRICS_COL_LINKS_TO_ROOTDMN', '8589934592');
  define('URLMETRICS_COL_ROOTDMN_LINKS_SUBDMN', '17179869184');
  define('URLMETRICS_COL_PAGE_AUTHORITY', '34359738368');
  define('URLMETRICS_COL_DOMAIN_AUTHORITY', '68719476736');
/**
 * The authentication class which is used to generate the authentication string
 * 
 * @author Radeep Solutions 
 */
class Authenticator {
  /**
   * accessID The user's Access ID
   */
  private $accessID;
  
  /**
   * secretKey The user's Secret Key 
   */
  private $secretKey;
  
  /**
   * expiresInterval The interval after which the authentication string expires
   * Default 300s 
   */
  private $expiresInterval = 300;
  
  /**
   * 
   * This method calculates the authentication String based on the 
   * user's credentials.
   * 
   * Set the user credentials before calling this method
   * 
   * @return the authentication string
   * 
   * @see #setAccessID(String)
   * @see #setSecretKey(String)
   */
    public function getAuthenticationStr() {
      
    $expires = time() + $this->expiresInterval;  

    $stringToSign = $this->accessID . "\n" . $expires;
  
    $binarySignature = hash_hmac('sha1', $stringToSign, $this->secretKey, TRUE);

    // We need to base64-encode it and then url-encode that.

    $urlSafeSignature = urlencode(base64_encode($binarySignature));
    
    $authenticationStr = "AccessID=" . $this->accessID . "&Expires=" . $expires . "&Signature=" . $urlSafeSignature;

    return $authenticationStr;
    }
    
  /**
   * @return the $accessID
   */
  public function getAccessID() {
    return $this->accessID;
  }

  /**
   * @return the $secretKey
   */
  public function getSecretKey() {
    return $this->secretKey;
  }

  /**
   * @param $accessID the $accessID to set
   */
  public function setAccessID($accessID) {
    $this->accessID = $accessID;
  }

  /**
   * @param $secretKey the $secretKey to set
   */
  public function setSecretKey($secretKey) {
    $this->secretKey = $secretKey;
  }
  /**
   * @return the $expiresInterval
   */
  public function getExpiresInterval() {
    return $this->expiresInterval;
  }

  /**
   * @param $expiresInterval the $expiresInterval to set
   */
  public function setExpiresInterval($expiresInterval) {
    $this->expiresInterval = $expiresInterval;
  }
}

/**
 * 
 * Utility Class to make a GET HTTP connection
 * to the given url and pass the output
 * 
 * @author Radeep Solutions
 *
 */
class ConnectionUtil {
  const CURL_CONNECTION_TIMEOUT = 120;
  
  /**
   * 
   * Method to make a GET HTTP connecton to 
   * the given url and return the output
   * 
   * @param urlToFetch url to be connected
   * @return the http get response
   */
  public static function makeRequest($urlToFetch) {

    if (FALSE) {
      $handle = fopen($urlToFetch, "r");
      $buffer = '';

      while (!feof($handle)) {
        $buffer .= fread($handle, 8192);
      }

      fclose($handle);

      $arr = json_decode($buffer);;

      return $arr;

    } 
    else {

      $curl_handle = curl_init();

      curl_setopt($curl_handle, CURLOPT_URL, "$urlToFetch");
      curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, ConnectionUtil::CURL_CONNECTION_TIMEOUT);
      curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        
      $buffer = curl_exec($curl_handle);
      //var_dump($buffer);
      curl_close($curl_handle);
    
      $arr = json_decode($buffer);
    
      return $arr;
    }
  }
  
}

/**
 * Service class to call the various methods to 
 * Anchor Text API 
 * 
 * Anchor Text api returns a set of anchor text terms of phrases aggregated across links to a page or domain.
 * 
 * @author Radeep Solutions
 *
 */
class AnchorTextService {
  private $authenticator;
  
  public function __construct($authenticator) {
    $this->authenticator = $authenticator;    
  }
  
  /**
   * This method returns a set of anchor text terms of phrases aggregated across links to a page or domain.
   * 
   * @param objectURL
   * @param scope determines the scope of the link, and takes one of the following values:
   *  phrase_to_page: returns a set of phrases found in links to the specified page
   *  phrase_to_subdomain: returns a set of phrases found in links to the specified subdomain
   *  phrase_to_domain: returns a set of phrases found in links to the specified root domain
   *  term_to_page: returns a set of terms found in links to the specified page
   *  term_to_subdomain: returns a a set of terms found in links to the specified subdomain
   *  term_to_domain: returns a a set of terms found in links to the specified root domain
   * @param sort determines the sorting of the links, in combination with limit and offset, this allows fast access to the top links by several orders:
   *  domains_linking_page: the phrases or terms found in links from the most number of root domains linking are returned first
   * @param col determines what fields are returned
   * @param offset The start record of the page can be specified using the Offset parameter
   * @param limit The size of the page can by specified using the Limit parameter. 
   * @return a set of anchor text terms of phrases aggregated across links to a page or domain.
   */
  public function getAnchorText($objectURL, $scope = NULL, $sort = NULL, $col = 0, $offset = -1, $limit = -1 ) {
    $urlToFetch = "http://lsapi.seomoz.com/linkscape/anchor-text/" . urlencode($objectURL) . "?" . $this->authenticator->getAuthenticationStr();
    
    if ($scope != NULL) {
      $urlToFetch = $urlToFetch . "&Scope=" . $scope;
    }
    if ($sort != NULL) {
      $urlToFetch = $urlToFetch . "&Sort=" . $sort;
    }
    if ($col > 0) {
      $urlToFetch = $urlToFetch . "&Cols=" . $col;
    }
    if ($offset >= 0) {
      $urlToFetch = $urlToFetch . "&Offset=" . $offset;
    }
    if ($limit >= 0) {
      $urlToFetch = $urlToFetch . "&Limit=" . $limit;
    }
    $response = ConnectionUtil::makeRequest($urlToFetch);
    
    return $response;
  }
  
  /**
   * @return the $authenticator
   */
  public function getAuthenticator() {
    return $this->authenticator;
  }

  /**
   * @param $authenticator the $authenticator to set
   */
  public function setAuthenticator($authenticator) {
    $this->authenticator = $authenticator;
  }
  
}


/**
 * 
 * Service class to call the various methods to 
 * Links API 
 * 
 * Links api returns a set of links to a page or domain.
 * 
 * @author Radeep Solutions
 *
 */
class LinksService {
  private $authenticator;
  
  public function __construct($authenticator) {
    $this->authenticator = $authenticator;    
  }
  
  /**
   * This method returns a set of links to a page or domain.
   * 
   * @param objectURL
   * @param scope determines the scope of the Target link, as well as the Source results.
   * @param filters  filters the links returned to only include links of the specified type.  You may include one or more of the following values separated by '+'
   * @param sort determines the sorting of the links, in combination with limit and offset, this allows fast access to the top links by several orders:
   * @param sourceCol specifies data about the source of the link is included
   * @param offset The start record of the page can be specified using the Offset parameter
   * @param limit The size of the page can by specified using the Limit parameter.
   * @return
   */
  public function getLinks($objectURL, $scope = NULL, $filters = NULL, $sort = NULL, $sourceCol = 0, $offset = -1, $limit = -1) {
    $urlToFetch = "http://lsapi.seomoz.com/linkscape/links/" . urlencode($objectURL) . "?" . $this->authenticator->getAuthenticationStr();
    
    if ($scope != NULL) {
      $urlToFetch = $urlToFetch . "&Scope=" . $scope;
    }
    if ($filters != NULL) {
      $urlToFetch = $urlToFetch . "&Filter=" . $filters;
    }
    if ($sort != NULL) {
      $urlToFetch = $urlToFetch . "&Sort=" . $sort;
    }
    if ($sourceCol > 0) {
      $urlToFetch = $urlToFetch . "&SourceCols=" . $sourceCol;
    }
    if ($offset >= 0) {
      $urlToFetch = $urlToFetch . "&Offset=" . $offset;
    }
    if ($limit >= 0) {
      $urlToFetch = $urlToFetch . "&Limit=" . $limit;
    }
    $response = ConnectionUtil::makeRequest($urlToFetch);
    
    return $response;
  }
  
  /**
   * @return the $authenticator
   */
  public function getAuthenticator() {
    return $this->authenticator;
  }

  /**
   * @param $authenticator the $authenticator to set
   */
  public function setAuthenticator($authenticator) {
    $this->authenticator = $authenticator;
  }
  
}


/**
 * Service class to call the various methods to
 * Top Pages Api
 * 
 * Top pages is a paid API that returns the metrics about many URLs on a given subdomain.
 * 
 * @author Radeep Solutions
 *
 */
class TopPagesService {
  private $authenticator;
  
  public function __construct($authenticator) {
    $this->authenticator = $authenticator;    
  }
  
  /**
   * This method returns the metrics about many URLs on a given subdomain
   * 
   * @param objectURL
   * @param col  A set of metrics can be requested by indicating them as bit flags in the Cols query parameter.
   * @param offset The start record of the page can be specified using the Offset parameter
   * @param limit The size of the page can by specified using the Limit parameter. 
   * @return
   */
  public function getTopPages($objectURL, $col = 0, $offset = -1, $limit = -1) {
    $urlToFetch = "http://lsapi.seomoz.com/linkscape/top-pages/" . urlencode($objectURL) . "?" . $this->authenticator->getAuthenticationStr();
    if ($offset >= 0) {
      $urlToFetch = $urlToFetch . "&Offset=" . $offset;
    }
    if ($limit >= 0) {
      $urlToFetch = $urlToFetch . "&Limit=" . $limit;
    }
    if ($col > 0) {
      $urlToFetch = $urlToFetch . "&Cols=" . $col;
    }
    
    $response = ConnectionUtil::makeRequest($urlToFetch);
    
    return $response;
  }

  /**
   * @return the $authenticator
   */
  public function getAuthenticator() {
    return $this->authenticator;
  }

  /**
   * @param $authenticator the $authenticator to set
   */
  public function setAuthenticator($authenticator) {
    $this->authenticator = $authenticator;
  }
}

/**
 * 
 * Service class to call the various methods to
 * URL Metrics
 * 
 * URL Metrics is a paid API that returns the metrics about a URL or set of URLs.  
 * 
 * @author Radeep Solutions
 *
 */
class URLMetricsService {
  private $authenticator;
  
  public function __construct($authenticator) {
    $this->authenticator = $authenticator;    
  }
  
  /**
   * 
   * This method returns the metrics about a URL or set of URLs.  
   * 
   * @param objectURL
   * @param col This field filters the data to get only specific columns
   *        col = 0 fetches all the data
   * @return
   */
  public function getUrlMetrics($objectURL, $col = 0) {
    
    $urlToFetch = "http://lsapi.seomoz.com/linkscape/url-metrics/" . urlencode($objectURL) . "?" . $this->authenticator->getAuthenticationStr();
    
    if ($col > 0) {
      $urlToFetch = $urlToFetch . "&Cols=" . $col;
    }
    $response = ConnectionUtil::makeRequest($urlToFetch);
    
    return $response;
  }
  
  /**
   * @return the $authenticator
   */
  public function getAuthenticator() {
    return $this->authenticator;
  }

  /**
   * @param $authenticator the $authenticator to set
   */
  public function setAuthenticator($authenticator) {
    $this->authenticator = $authenticator;
  }
  
}
